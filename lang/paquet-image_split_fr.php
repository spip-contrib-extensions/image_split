<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// s
	'image_split_description' => 'Cr&eacute;er deux images superpos&eacute;es de type avant/apr&egrave;s, avec un slider.',
	'image_split_nom' => 'Image Split',
);

?>