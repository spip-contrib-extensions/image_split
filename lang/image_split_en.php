<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(
		// A
		'after_label' => 'Second image label: ',
		'ajuster' => 'Adjust!',
		'ajuster_split' => 'Adjust image split',
		
		// B
		'back' => 'Back',
		'before_label' => 'First image label: ',
		
		// C
		'click_to_move' => 'If we click on an image, move the slider to that location: ',
		'configurer_titre' => 'Image split settings',
		'crop_it' => 'Crop it!',
		
		// H
		'horizontal' => 'Horizontally',
		
		// I
		'image' => 'Image',
		
		// M
		'move_slider_on_hover' => 'Move the slider when hovering the images: ',
		'move_with_handle_only' => 'Only move the slider when we select it: ',
		
		// N
		'non' => 'No',
		
		// O
		'offset' => 'Slider offset',
		'offset_explanation' => 'This is the position of the slider/divider, or the proportion of the first picture visible when the plugin initializes.',
		'offset_label' => 'Offset: ',
		'orientation' => 'Orientation',
		'orientation_explanation' => 'In which direction would you like to split the images?',
		'oui' => 'Yes',
		'overlay_labels' => 'Overlay and labels',
		'overlay_labels_explanation' => 'You can add a slightly darker overlay with labels for each image when hovering the images.',
		
		// S
		'settings_explanation' => 'This is the default setup for all image splits in the website. You can however overwrite those settings for each set of images (see <a href="https://contrib.spip.net/Image-Split-5084">documentation</a>).',
		'show_overlay' => 'Show overlay and labels: ',
		'slider_setup' => 'Slider settings',
		'slider_setup_explanation' => 'You can adjust the behaviour of the slider that separates the images here.',
		
		// T
		'tips' => '<strong>Tips:</strong>
		<br>- Switch from one picture to another by clicking on the tabs.
		<br>- Adjust the crop by draging an image and scrolling the mouse.
		<br>- Make sure the crop box (blue frame) is always inside both pictures and that there is no empty gap, otherwise it wont crop properly.
		<br>- You can resize the crop box, but it is mostly to set the ratio of the resulting pictures. Making the crop box bigger does not guaranty a bigger result. The real cropping adjustment is made by moving and zooming the 2 images.
		<br>- When both images are well superposed, click on "Crop it!".',
		
		// V
		'vertical' => 'Vertically',
		
);

?>