<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

		// A
		'after_label' => 'Label de la seconde image: ',
		'ajuster' => 'Ajuster!',
		'ajuster_split'=> 'Ajuster image split',
		
		// B
		'back' => 'Retour',
		'before_label' => 'Label de la premi&egrave;re image: ',
		
		// C
		'click_to_move' => 'Si on clique sur une image, d&eacute;placer le slider &agrave; cet endroit: ',
		'configurer_titre' => 'Configuration de image split',
		'crop_it' => 'Recadrer!',
		
		// H
		'horizontal' => 'Horizontalement',
		
		// I
		'image' => 'Image',
		
		// M
		'move_slider_on_hover' => 'D&eacute;placer le slider quand on survole les images: ',
		'move_with_handle_only' => 'Ne d&eacute;placer le slider que quand on le selectionne: ',
		
		// N
		'non' => 'Non',
		
		// O
		'offset' => 'Position du slider',
		'offset_explanation' => 'Il s\'agit de la position du slider/s&eacute;parateur. Autrement dit, il s\'agit de la proportion visible de la premi&egrave;re image lors de l\'initialisation du plugin.',
		'offset_label' => 'Position: ',
		'orientation' => 'Orientation',
		'orientation_explanation' => 'Dans quel sens souhaitez-vous s&eacute;parer les images?',
		'oui' => 'Oui',
		'overlay_labels' => 'Couverture et labels',
		'overlay_labels_explanation' => 'Vous pouvez ajouter une couverture l&eacute;ger&egrave;ment sombre ainsi que des labels pour chaque image.',
		
		// S
		'settings_explanation' => 'Vous pouvez d&eacute;finir ici la configuration par d&eacute;faut de l\'ensemble des images splits de votre site. Il est toutefois possible de surcharger ces options pour chaque occurence (voir <a href="https://contrib.spip.net/Image-Split">documentation</a>).',
		'show_overlay' => 'Afficher la couverture et les labels: ',
		'slider_setup' => 'Configuration du slider',
		'slider_setup_explanation' => 'Vous pouvez r&eacute;gler ici le comportement du slider qui s&eacute;pare les images.',
		
		// T
		'tips' => '<strong>Instructions:</strong>
		<br>- Passez d\'une image &agrave; l\'autre en cliquant sur les onglets.
		<br>- Ajuster chaque crop en faisant glisser une image et en utilisant la roulette de votre souris.
		<br>- Assurez-vous que la superficie du crop (cadre bleu) soit toujours &agrave; l\'int&eacute;rieur des deux images et qu\'il n\'y a aucun espace vide, afin d\'&eacute;viter des erreurs de crop.
		<br>- Vous pouvez redimensionner la superficie du crop, Mais elle est surtout utilis&eacute;e pour definir le ratio des images qui vont &ecirc;tre cr&eacute;&eacute;es. Agrandir la zone de crop ne va pas garantir de plus grandes images en r&eacute;sultat. La r&eacute;elle d&eacute;finition des crops se fait en glissant et zoomant les images.
		<br>- Quand les deux images sont correctement superpos&eacute;es, cliquez sur "Recadrer!".',
		
		// V
		'vertical' => 'Verticallement',
		
);

?>